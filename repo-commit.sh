
if [ $# -le 0 ]
then
  echo "Commit reason not specified!"
else
  echo "BZR:"
  bzr commit -m "$1"
  echo "GIT:"
  git commit -m "$1"
  echo "DONE!"
fi
