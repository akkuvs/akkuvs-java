
function AkkuVSWeb()
{
}


AkkuVSWeb.Pages = ['MainPage', 'BatteryList', 'BatteryPage', 'NewBatteryPage', 'QuickTaskPage'];

AkkuVSWeb.Init = function()
{
  var Form = document.getElementById('LoginForm');
  Form.style.display = "block";
  //alert("Test");
};


AkkuVSWeb.ShowPage = function(PageName)
{
  if(PageName != undefined)
  {
    for(var i = 0; i < AkkuVSWeb.Pages.length; i++)
    {
      //TODO: optimize: replace getElementById calls
      if(PageName == AkkuVSWeb.Pages[i])
      {
        document.getElementById(AkkuVSWeb.Pages[i]).style.display = "block";
      }
      else
      {
        document.getElementById(AkkuVSWeb.Pages[i]).style.display = "none";
      }
    }
  }
}

AkkuVSWeb.Login = function()
{
  var User = document.forms.Login.User.value;
  var Pass = document.forms.Login.Password.value;
  var LoginResponse = RequestHandler.Login(User, Pass);
  if(LoginResponse.Success == false)
  {
    //TODO: Write function for error displaying
    alert(LoginResponse.Error);
  }
  else
  {
    document.getElementById('LoginForm').style.display = "none";
    document.getElementById('MainContent').style.display = "block";
    document.getElementById('Navigation').style.display = "inline";
  }
};


AkkuVSWeb.Logout = function()
{
  if(RequestHandler.Logout() == true)
  {
    document.getElementById('LoginForm').style.display = "block";
    document.getElementById('MainContent').style.display = "none";
    document.getElementById('Navigation').style.display = "none";
    
    AkkuVSWeb.ShowPage('MainPage');
    
    //clear content of all forms:
    for(var i = 0; i<document.forms.length; i++)
    {
      document.forms[i].reset();
    }
  }
  else
  {
    alert("Logout not successful!");
  }
};


AkkuVSWeb.GetBattery = function(BatteryID)
{
  if(BatteryID != undefined)
  {
    var Request = [{
      "Function":"GetBattery",
      "Parameters":[BatteryID.toString(), "true"]
      }];
    var Response = RequestHandler.SendRequest(Request);
    var Battery = Response[0].Data[0];
    if(RequestHandler.ValidateRequest(Battery) != true)
    {
      return null;
    }
    else
    {
      document.getElementById('BP_ID').innerHTML = Battery.ID;
      document.getElementById('BP_RegNr').innerHTML = Battery.RegNr;
      document.getElementById('BP_Name').innerHTML = Battery.Name;
      document.getElementById('BP_Technology').innerHTML = Battery.Technology;
      document.getElementById('BP_Size').innerHTML = Battery.Size;
      document.getElementById('BP_Voltage').innerHTML = Battery.Voltage;
      document.getElementById('BP_Cells').innerHTML = Battery.Cells;
      document.getElementById('BP_Capacity').innerHTML = Battery.Capacity;
      document.getElementById('BP_RegistrationDate').innerHTML = Battery.RegistrationDate.date;
    }
  }
  
  AkkuVSWeb.ShowPage('BatteryPage');
};

AkkuVSWeb.CreateBattery = function()
{
  var form = document.forms.F_NewBattery;
  
  var battery = new Battery();
  battery.ID = -1;
  battery.RegNr = form.NB_RegNr.value;
  battery.Name = form.NB_Name.value;
  battery.Charged = false;
  battery.Technology = new Technology();
  battery.Technology.TechnologyID = form.NB_Technology.value;
  battery.Technology.TechnologyName = "";
  battery.Size = new Size();
  battery.Size.SizeID = form.NB_Size.value;
  battery.Size.SizeName = "";
  battery.Voltage = form.NB_Voltage.value;
  battery.Cells = form.NB_Cells.value;
  battery.Capacity = form.NB_Capacity.value;
  battery.RegistrationDate = form.NB_RegistrationDate.value;
  
  var Request = [{
    "Function":"CreateBattery",
    "Parameters":[ JSON.stringify(battery) ]
    }];
    
  var Result = RequestHandler.SendRequest(Request);
  if(Result.ErrorCode != 200)
  {
    window.alert("Error when creating the battery!");
  }
};


AkkuVSWeb.ShowNewBatteryPage = function()
{
  var TechSelect = document.getElementById('NB_Technology');
  var SizeSelect = document.getElementById('NB_Size');
  
  //read all avaibale technologies and empty the select box first:
  TechSelect.innerHTML = "";
  /* not working:
  while(TechSelect.fistChild)
  {
    TechSelect.removeChild(TechSelect.firstChild);
  }*/
  
  var TechList = AkkuVSWeb.GetTechnologyList();
  if(TechList != undefined)
  {
    for(var i = 0; i<TechList.length; i++)
    {
      var option = document.createElement("option");
      var text = document.createTextNode(TechList[i].TechnologyName);
      option.appendChild(text);
      option.setAttribute("value", TechList[i].TechnologyID);
      TechSelect.appendChild(option);
    }
  }
  
  //read all avaibale sizes and empty the select box first:
  SizeSelect.innerHTML = "";
  /* not working:
  while(SizeSelect.fistChild)
  {
    SizeSelect.removeChild(SizeSelect.firstChild);
  }
  */
  
  var SizeList = AkkuVSWeb.GetSizeList();
  if(SizeList != undefined)
  {
    for(var i = 0; i<SizeList.length; i++)
    {
      var option = document.createElement("option");
      var text = document.createTextNode(SizeList[i].SizeName);
      option.appendChild(text);
      option.setAttribute("value", SizeList[i].SizeID);
      SizeSelect.appendChild(option);
    }
  }
  
  AkkuVSWeb.ShowPage('NewBatteryPage');
};


AkkuVSWeb.GetBatteryList = function()
{
  var Request = [{
    "Function":"GetBatteryList",
    "Parameters":[]
    }];
  
  var Response = RequestHandler.SendRequest(Request);
  var BatteryList = Response[0].Data;
  
  var Table = document.getElementById('Table_BatteryList');
  
  Table.innerHTML = "";
  
  for(var i = 0; i<BatteryList.length; i++)
  {
    var Row = document.createElement("tr");
    var Item = BatteryList[i];
    //TODO: use DOM methods instead of innerHTML
    
    Row.innerHTML = '<td>'
      +'<a href="javascript:void(AkkuVSWeb.GetBattery(\''
      +Item.ID+'\'));">'
      +Item.ID+'</a></td><td>'+
                    Item.RegNr+'</td><td>'+
                    Item.Name+'</td><td>'+
                    Item.Capacity+'</td><td>'+
                    Item.Voltage+'</td><td>'+
                    Item.Technology.TechnologyName+'</td><td>'+
                    Item.Size.SizeName+'</td><td>'+
                    Item.Cells+'</td><td>'+
                    Item.RegistrationDate+'</td>';
                    
    Table.appendChild(Row);
  }
  
  AkkuVSWeb.ShowPage('BatteryList');
  /*
  document.getElementById('BatteryList').style.display = "block";
  document.getElementById('BatteryPage').style.display = "none";
  document.getElementById('NewBatteryPage').style.display = "none";
  */
};


AkkuVSWeb.GetTechnologyList = function()
{
  var Request = [{
    "Function":"GetTechnologyList",
    "Parameters":[]
    }];
  
  var Response = RequestHandler.SendRequest(Request);
  var TechList = Response[0].Data;
  return TechList;
};


AkkuVSWeb.GetSizeList = function()
{
  var Request = [{
    "Function":"GetSizeList",
    "Parameters":[]
    }];
  
  var Response = RequestHandler.SendRequest(Request);
  var SizeList = Response[0].Data;
  return SizeList;
};


AkkuVSWeb.ChangeQuickTask = function(select)
{
  document.getElementById('FQT_Charge').style.display = "none";
  document.getElementById('FQT_Discharge').style.display = "none";
  document.getElementById('FQT_State').style.display = "none";
  document.getElementById('FQT_Usage').style.display = "none";
  switch(select.value)
  {
    case 'C':
    {
      document.getElementById('FQT_Charge').style.display = "block";
      break;
    }
    case 'D':
    {
      document.getElementById('FQT_Discharge').style.display = "block";
      break;
    }
    case 'S':
    {
      document.getElementById('FQT_State').style.display = "block";
      break;
    }
    case 'U':
    {
      document.getElementById('FQT_Usage').style.display = "block";
      break;
    }
    default:
    {
      document.getElementById('FQT_Charge').style.display = "block";
      break;
    }
  }
};


AkkuVSWeb.HandleQuickTask = function(form)
{
  alert("Quick Task functions not yet implemented!");
};

//window.onload = AkkuVSWeb.Init;

//for development purposes:
window.onload = function()
{
  AkkuVSWeb.Init();
}
