
function RequestHandler()
{
}


RequestHandler.ValidateRequest = function(Request)
{
  //STUB, unimplemented!
  return true;
}

RequestHandler.SendRequest = function(Request)
{
  var Result = null;
  var Ajax = new XMLHttpRequest();
  try
  {
    Ajax.open("POST",AkkuVSWeb.Configuration.BackendPath, false);
    Ajax.send(JSON.stringify(Request));
  }
  catch(e)
  {
    return null;
  }
  
  if(Ajax.readyState == 4 && Ajax.status == 200)
  {
    try
    {
      Result = JSON.parse(Ajax.responseText);
    }
    catch(e)
    {
      return null;
    }
  }
  
  return Result;
};


RequestHandler.Login = function(User, Pass)
{
  var Request = [{
    "Function":"Login",
    "Parameters":[User, Pass]
    }];
  var Result = RequestHandler.SendRequest(Request);
  if(Result == null || Result == undefined)
  {
    var LoginResponse = {
          "Success":false,
          "Error":"Frontend: No Data received!"};
    return LoginResponse;
  }
  else
  {
    switch(Result[0].ErrorCode)
    {
      case 200:
      {
        var LoginResponse = {
        "Success":true,
        "Error":""};
        return LoginResponse;
        break;
      }
      case 401:
      {
        var LoginResponse = {
        "Success":true,
        "Error":Result[0].ErrorValue};
        return LoginResponse;
        break;
      }
      default:
      {
        var LoginResponse = {
        "Success":false,
        "Error":"Code "+Result[0].ErrorCode+": "+Result[0].ErrorValue};
        return LoginResponse;
        break;
      }
    }
  }
};


RequestHandler.Logout = function()
{
  var Request = [{
    "Function":"Logout",
    "Parameters":[]
    }];
  var Result = RequestHandler.SendRequest(Request);
  if(Result == null || Result == undefined)
  {
    var LoginResponse = {
          "Success":false,
          "Error":"Frontend: No Data received!"};
    return LoginResponse;
  }
  else
  {
    if(Result[0].ErrorCode == 200)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
};