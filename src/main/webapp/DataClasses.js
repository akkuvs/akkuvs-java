
function AkkuVSRequest()
{
  this.Function = "";
  this.Parameters = [];
};

function AkkuVSResult()
{
  this.ErrorCode = 0;
  this.ErrorValue = "";
  this.Data = [];
};


function Battery()
{
  this.ID = 0;
  this.RegNr = "";
  this.Name = "";
  this.Technology = "";
  this.Size = "";
  this.Voltage = 0.0;
  this.Cells = 0;
  this.Capacity = 0.0;
  this.RegistrationDate = new Date(0);
  this.Charged = false;
  this.Charges = [];
  this.Discharges = [];
  this.Usages = [];
  this.States = [];
  
  this.Groups = [];
  
};


function Technology()
{
  this.TechnologyID = 0;
  this.TechnologyName = "";
};


function Size()
{
  this.SizeID = 0;
  this.SizeName = "";
};


function Charge()
{
  this.Date = new Date(0);
  this.Current = 0.0;
  this.MeasuredCap = 0.0;
  this.Recharged = false;
};


function Discharge()
{
  this.Date = new Date(0);
};

function State()
{
  this.StateStartDate = new Date(0);
  this.StateID = 0;
  this.StateName = "";
};

function Usage()
{
  this.UsageID = 0;
  this.UsageName = "";
  this.StartDate = new Date(0);
  this.Location = "";
};