package mstrohm.akkuvs;


import mstrohm.akkuvs.databases.DatabaseBackend;
import mstrohm.akkuvs.dataclasses.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.hibernate.Criteria;
import org.hibernate.Session;




public class ApiServlet extends HttpServlet
{
	/**
	 * development stage only: use hibernate wherever it is implemented.
	 * useHibernate is only for testing purposes and will be removed
	 * when AkkuVS is more advanced and in a more usuable state.
	 */
	private static final boolean useHibernate = true;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException
	{
		PrintWriter out = resp.getWriter();
		out.println("[{\"ErrorCode\":501,\"ErrorValue\":\"Method not implemented: HTTP GET\",\"Result\":[]}]");
		//HTTP code 501: Method not implemented
	  
		out.close();
		//out.flush();
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException
	{
		HttpSession session = req.getSession(true);
		//TODO: make session synchronized

		//Cookie sessionCookie = new Cookie("jsessionid",session.getId());
		//resp.addCookie(sessionCookie);
		
		resp.setHeader("Access-Control-Allow-Origin", "*");
		//the above header is important, if the client is web browser based
		//and not located on the same host than this servlet
		//resp.setHeader("Access-Control-Allow-Headers", "Content-Type");
		
		
		String resultStr = "[";
		
		if(session.getAttribute("DBBackend") == null)
		{
			try
			{
				session.setAttribute("DBBackend", new DatabaseBackend("org.postgresql.Driver"));
			}
			catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try
		{
			JSONArray avsRequest = new JSONArray(req.getReader().readLine());
			for(int i = 0; i < avsRequest.length(); i++)
			{
				//Request newRequest = (Request) avsRequest.get(i);
				try
				{
					Request newRequest = Request.parseFromJSON(avsRequest.getJSONObject(i));
					Result requestResult;
					try
					{
						requestResult = this.handleRequest(session, newRequest);
						//convert to JSON and send
						if(i>0)
						{
							resultStr += ",";
						}
						resultStr += requestResult.toJSON();
					}
					catch (RequestHandlingException e)
					{
						System.err.println("Error when handling "+newRequest.Function+": "+ e.reason);
						e.printStackTrace();
						
						//something went wrong: send code 500 (Internal server error)
						if(i>0)
						{
							resultStr += ",";
						}
						resultStr += "{\"ErrorCode\":500, \"ErrorValue\":\"Request could not be answered!\", \"Data\":[]}";
					}
					
				}
				catch(JSONException e)
				{
					//newRequest == null: bad request 
					if(i>0)
					{
						resultStr += ",";
					}
					resultStr += "{\"ErrorCode\":400, \"ErrorValue\":\"Request is not in the request object format as specified in the API!\", \"Data\":[]}";
				}
			}
			resultStr += "]";
		}
		catch (JSONException e)
		{
			resultStr = "[{\"ErrorCode\":400, \"ErrorValue\":\"Requests must be made of one JSON array containing several request objects!\", \"Data\":[]}]";
			e.printStackTrace();
		}
		
		
		//String resultStr = "[{\"ErrorCode\":503, \"ErrorValue\":\"POST: Not yet implemented!\", \"Data\":[]}]";
		
		PrintWriter out2 = resp.getWriter();
		out2.println(resultStr);
		out2.close();
		resp.flushBuffer();
		
	}
	
	
	private Result handleRequest(HttpSession session, Request newRequest) throws RequestHandlingException
	{
		DatabaseBackend dbBackend = (DatabaseBackend) session.getAttribute("DBBackend");
		if(dbBackend == null)
		{
			throw new RequestHandlingException();
		}
			
		// if...else instead of switch, for compatibility with Java 6
		if(newRequest.Function.compareTo("Login") == 0)
		{
			//DEVELOPMENT ONLY: Log the user in, don't care about the password
			if(dbBackend.login("jdbc:postgresql://127.0.0.1/AkkuVS", "akkuvs", "akkuvs") != true)
			
			//replace the if-statement above with the following one to get code that is more usuable for the real world:
			//if(dbBackend.login("jdbc:postgresql://127.0.0.1/AkkuVS", newRequest.Parameters.get(0), newRequest.Parameters.get(1)) != true)
			{
				throw new RequestHandlingException();
			}
			Result loginResult = new Result();
			loginResult.ErrorCode = 200;
			loginResult.ErrorValue = "Login";
			//login has no data, so Data won't be set to a new empty ArrayList ;-)
			
			return loginResult;
		}
		
		
		else if(newRequest.Function.compareTo("Logout") == 0)
		{
			if(dbBackend.logout() != true)
			{
				throw new RequestHandlingException();
			}
			
			Result logoutResult = new Result();
			logoutResult.ErrorCode = 200;
			logoutResult.ErrorValue = "Logout";
			return logoutResult;
		}
		
		
		else if(newRequest.Function.compareTo("GetBatteryList") == 0)
		{
			ArrayList<Battery> batteryList = dbBackend.readBatteryList();
			
			Result batteryListResult = new Result();
			
			if(batteryList == null)
			{
				batteryListResult.ErrorCode = 500;
				batteryListResult.ErrorValue = "Error reading battery list!";
				batteryListResult.Data = new ArrayList<DataClass>();
			}
			else
			{
				batteryListResult.ErrorCode = 200;
				batteryListResult.ErrorValue = "GetBatteryList";
				batteryListResult.Data = new ArrayList<DataClass>();
				for(int i = 0; i<batteryList.size(); i++)
				{
					batteryListResult.Data.add(batteryList.get(i));
				}
			}
			return batteryListResult;
		}
		
		
		else if(newRequest.Function.compareTo("GetSizeList") == 0)
		{
			Session hibernateSession = HibernateHelper.getSessionFactory().getCurrentSession();
			hibernateSession.beginTransaction();
			
			Criteria allSizes = hibernateSession.createCriteria(Size.class);
			
			List<Object> sizeList = allSizes.list();
			Iterator<Object> sizeListIterator = sizeList.iterator();
			
			Result sizeListResult = new Result();
			
			if(sizeList.isEmpty())
			{
				sizeListResult.ErrorCode = 500;
				sizeListResult.ErrorValue = "Error: No sizes available!";
				sizeListResult.Data = new ArrayList<DataClass>();
			}
			else
			{
				sizeListResult.ErrorCode = 200;
				sizeListResult.ErrorValue = "GetSizeList";
				sizeListResult.Data = new ArrayList<DataClass>();
				
				while(sizeListIterator.hasNext())
				{
					sizeListResult.Data.add((Size) sizeListIterator.next());
				}
			}
			
			hibernateSession.getTransaction().commit();
			
			/*
			ArrayList <Size> sizeList = dbBackend.readSizeList();
			Result sizeListResult = new Result();
			if(sizeList == null)
			{
				sizeListResult.ErrorCode = 500;
				sizeListResult.ErrorValue = "Error reading size list!";
				sizeListResult.Data = new ArrayList<DataClass>();
			}
			else
			{
				sizeListResult.ErrorCode = 200;
				sizeListResult.ErrorValue = "GetSizeList";
				sizeListResult.Data = new ArrayList<DataClass>();
				for(int i = 0; i<sizeList.size(); i++)
				{
					sizeListResult.Data.add(sizeList.get(i));
				}
			}
			*/
			return sizeListResult;
		}
		
		
		else if(newRequest.Function.compareTo("GetTechnologyList") == 0)
		{
			Session hibernateSession = HibernateHelper.getSessionFactory().getCurrentSession();
			hibernateSession.beginTransaction();
			
			Criteria allTechnologies = hibernateSession.createCriteria(Technology.class);
			
			List<Object> technologyList = allTechnologies.list();
			Iterator<Object> technologyListIterator = technologyList.iterator();
			
			/*System.err.println();
			System.err.println("Read " + Integer.toString(technologyList.size()) + " technologies via Hibernate!");
			System.err.println();*/
			
			Result techListResult = new Result();
			if(technologyList.isEmpty())
			{
				techListResult.ErrorCode = 500;
				techListResult.ErrorValue = "Error: No technologies available!";
				techListResult.Data = new ArrayList<DataClass>();
			}
			else
			{
				techListResult.ErrorCode = 200;
				techListResult.ErrorValue = "GetTechnologyList";
				techListResult.Data = new ArrayList<DataClass>();
				
				while(technologyListIterator.hasNext())
				{
					techListResult.Data.add((Technology) technologyListIterator.next());
				}
			}
			hibernateSession.getTransaction().commit();
			
			return techListResult;
			
			/*
			ArrayList <Technology> techList = dbBackend.readTechnologyList();
			Result techListResult = new Result();
			if(techList == null)
			{
				techListResult.ErrorCode = 500;
				techListResult.ErrorValue = "Error reading technology list!";
				techListResult.Data = new ArrayList<DataClass>();
			}
			else
			{
				techListResult.ErrorCode = 200;
				techListResult.ErrorValue = "GetTechnologyList";
				techListResult.Data = new ArrayList<DataClass>();
				for(int i = 0; i<techList.size(); i++)
				{
					techListResult.Data.add(techList.get(i));
				}
			}
			return techListResult;
			*/
		}
		
		
		else if(newRequest.Function.compareTo("CreateBattery") == 0)
		{
			Result createResult = new Result();
			try
			{
				JSONObject jsonBattery = new JSONObject(newRequest.Parameters.get(0));
				
				Battery newBattery = new Battery();
				newBattery.parseFromJson(jsonBattery);
				
				boolean success = dbBackend.createBattery(newBattery);
				if(success)
				{
					createResult.ErrorCode = 200;
					createResult.ErrorValue = "CreateBattery";
				}
				else
				{
					createResult.ErrorCode = 500;
					createResult.ErrorValue = "Error when creating new battery!";
				}
				return createResult;
			}
			catch (JSONException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				createResult.ErrorCode = 500;
				createResult.ErrorValue = "Error when creating new battery!";
				return createResult;
			}
		}
		
		else if(newRequest.Function.compareTo("GetBattery") == 0)
		{
			Result batteryResult = new Result();
			if(newRequest.Parameters.size() != 2)
			{
				batteryResult.ErrorCode = 400;
				batteryResult.ErrorValue = "Invalid amount of parameters! 2 expected, " + newRequest.Parameters.size() + " parameters received!";
				
			}
			else
			{
				int batteryID = Integer.parseInt(newRequest.Parameters.get(0));
				boolean additionalData = Boolean.getBoolean(newRequest.Parameters.get(1));
				Battery b = dbBackend.readBatteryByID(batteryID, additionalData);
				if(b != null)
				{
					batteryResult.ErrorCode = 200;
					batteryResult.ErrorValue = "GetBattery";
					batteryResult.Data = new ArrayList<DataClass>();
					batteryResult.Data.add(b);
					
				}
			}
			return batteryResult;
		}
		
		
		else
		{
			throw new RequestHandlingException("Function not implemented!");
		}
	}
}
