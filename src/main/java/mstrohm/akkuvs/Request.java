package mstrohm.akkuvs;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;;

public class Request
{
	public String Function;
	public ArrayList<String> Parameters;
	
	static Request parseFromJSON(JSONObject json) throws JSONException
	{
		String Function = json.getString("Function");
		ArrayList<String> Parameters = new ArrayList<String>();
		JSONArray jsonParameters = json.getJSONArray("Parameters");
		for(int i = 0; i < jsonParameters.length(); i++)
		{
			Parameters.add(jsonParameters.getString(i));
		}
		
		//no exception until here? Fine! Let's return a Request object:
		
		Request parsedRequest = new Request();
		parsedRequest.Function = Function;
		parsedRequest.Parameters = Parameters;
		
		return parsedRequest;
	}
	
	public Request()
	{
		this.Function = "";
		this.Parameters = new ArrayList<String>();
	}
	
}
