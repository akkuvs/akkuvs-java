package mstrohm.akkuvs.databases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;


import mstrohm.akkuvs.dataclasses.*;


public class DatabaseBackend
{
	private Connection connection;
	public DatabaseBackend(String driverClass) throws Exception
	{
		Class.forName(driverClass);
	}
	
	public Connection getConnection()
	{
		return this.connection;
	}
	
	public boolean login(String database, String username, String password)
	{
		try
		{
			this.connection = DriverManager.getConnection(database, username, password);
			return true;
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean logout()
	{
		try
		{
			if(this.connection != null)
			{
				this.connection.close();
			}
			return true;
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public ArrayList<Battery> readBatteryList()
	{
		if(this.connection != null)
		{
			try
			{
				Statement stmt = this.connection.createStatement();
				stmt.executeQuery("select B.BatteryID, B.RegistrationNumber, B.Name, B.Technology, T.Name, B.Size, S.Name, B.Voltage, B.Cells, B.Capacity, B.RegDate "
						+ "from Battery as B, Sizes as S, Technology as T "
						+ "where B.Technology = T.TechnologyID "
						+ "and B.Size = S.SizeID;");
				ResultSet result = stmt.getResultSet();
				
				ArrayList <Battery> resultList = new ArrayList<Battery>();
				//read rows one by one:
				while(result.next())
				{
					Size bSize = new Size(result.getInt(6), result.getString(7));
					Technology bTech = new Technology(result.getInt(4), result.getString(5));
					
					Battery b =	new Battery(
						result.getInt(1),
						result.getString(2),
						result.getString(3),
						//TODO: convert integer to specified technology and size name:
						//(by using join on tables)
						bTech,
						bSize,
						result.getFloat(8),
						result.getInt(9),
						result.getFloat(10),
						result.getDate(11)
						);
					
					//STUB!
					//TODO: read maximum from charges and dischages and compare
					b.charged = false;
					
					resultList.add(b);
				}
				return resultList;
			}
			catch (SQLException e)
			{
				e.printStackTrace();
				return null;
			}
		}
		return null;
		
	}

	public ArrayList<Size> readSizeList()
	{
		if(this.connection != null)
		{
			try
			{
				Statement stmt = this.connection.createStatement();
				stmt.executeQuery("select SizeID, Name "
						+ "from Sizes;");
				ResultSet result = stmt.getResultSet();
				
				ArrayList<Size> resultList = new ArrayList<Size>();
				//read rows one by one:
				while(result.next())
				{
					resultList.add(
							new Size(
									result.getInt(1),
									result.getString(2)
									)
							);
				}
				return resultList;
			}
			catch (SQLException e)
			{
				e.printStackTrace();
				return null;
			}
		}
		else
		{
			return null;
		}
	}

	public ArrayList<Technology> readTechnologyList()
	{
		if(this.connection != null)
		{
			try
			{
				Statement stmt = this.connection.createStatement();
				stmt.executeQuery("select ID, Name "
						+ "from Technology;");
				ResultSet result = stmt.getResultSet();
				
				ArrayList<Technology> resultList = new ArrayList<Technology>();
				//read rows one by one:
				while(result.next())
				{
					resultList.add(
							new Technology(
									result.getInt(1),
									result.getString(2)
									)
							);
				}
				return resultList;
			}
			catch (SQLException e)
			{
				e.printStackTrace();
				return null;
			}
		}
		else
		{
			return null;
		}
	}

	public boolean createBattery(Battery newBattery)
	{
		if(this.connection != null)
		{
			try
			{
				//TODO: switch to a timezone-aware date format onto the database
				//or eliminate the timezone from all other parts of the software
				
				DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Statement stmt = this.connection.createStatement();
				String sqlcode = "insert into Battery "
						+ "(RegistrationNumber, Name, Technology, Size, Voltage, Cells, Capacity, RegDate) " 
						+ "values (\""
						+ newBattery.regNr + "\",\"" 
						+ newBattery.name + "\","
						+ newBattery.technology.techId + ","
						+ newBattery.size.sizeId + ","
						+ newBattery.voltage + ","
						+ newBattery.cells + ","
						+ newBattery.capacity + ",\""
						+ dateFormatter.format(newBattery.registrationDate)
						+ "\""
						+ ");";
				
				System.err.println("DEBUG: "+sqlcode);
				int result = stmt.executeUpdate(sqlcode);
						
				if(result == 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch (SQLException e)
			{
				e.printStackTrace();
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	public Battery readBatteryByID(int batteryID, boolean additionalData) {
		if(this.connection != null)
		{
			try
			{
				Statement stmt = this.connection.createStatement();
				stmt.executeQuery("select BatteryID, RegistrationNumber, Battery.Name as Name, T.Name as Technology, S.Name as Size, Voltage, Cells, Capacity, RegDate "
						+ "from Battery, Technology as T, Sizes as S "
						+ "where Battery.Technology = T.TechnologyID and "
						+ "Battery.Size = S.SizeID"
						);
				ResultSet result = stmt.getResultSet();
				
				Battery b = null;
				//read rows one by one:
				if(result.next())
				{
					b = new Battery(result.getInt(1), result.getString(2), result.getString(3), new Technology(result.getString(4)), new Size(result.getString(5)), result.getFloat(6), result.getInt(7), result.getFloat(8), result.getDate(9));
					//TODO: calculation if battery is charged
					b.charged = false;
				}
				
				//TODO: read additional data, if wished
				
				
				return b;
			}
			catch (SQLException e)
			{
				e.printStackTrace();
				return null;
			}
		}
		else
		{
			return null;
		}
	}
}
