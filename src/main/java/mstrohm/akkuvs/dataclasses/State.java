package mstrohm.akkuvs.dataclasses;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

public class State implements DataClass
{
	
	public int stateId;
	public String stateName;
	public int stateTypeId;
	public Date stateStartDate;
	
	@Override
	public void parseFromJson(JSONObject json)
	{
		try
		{
			this.stateId = json.getInt("StateID");
			this.stateName = json.getString("StateName");
			DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
			this.stateStartDate = dateFormatter.parse(json.getString("StateStartDate"));
		}
		catch(ParseException e)
		{
			e.printStackTrace();
		}
		catch(JSONException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public String toJSON(boolean allData)
	{
		return "{}";
	}

	/**
	 * @return the stateId
	 */
	public int getStateId() {
		return stateId;
	}

	/**
	 * @param stateId the stateId to set
	 */
	public void setStateId(int stateId) {
		this.stateId = stateId;
	}

	/**
	 * @return the stateName
	 */
	public String getStateName() {
		return stateName;
	}

	/**
	 * @param stateName the stateName to set
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * @return the stateTypeId
	 */
	public int getStateTypeId() {
		return stateTypeId;
	}

	/**
	 * @param stateTypeId the stateTypeId to set
	 */
	public void setStateTypeId(int stateTypeId) {
		this.stateTypeId = stateTypeId;
	}

	/**
	 * @return the stateStartDate
	 */
	public Date getStateStartDate() {
		return stateStartDate;
	}

	/**
	 * @param stateStartDate the stateStartDate to set
	 */
	public void setStateStartDate(Date stateStartDate) {
		this.stateStartDate = stateStartDate;
	}
	
}
