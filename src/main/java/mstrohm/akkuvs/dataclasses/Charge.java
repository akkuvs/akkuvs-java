package mstrohm.akkuvs.dataclasses;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

public class Charge implements DataClass
{
	public int chargeId;
	public Date date;
	public float current;
	public float measuredCapacity;
	public boolean recharged;
	
	
	@Override
	public String toJSON(boolean allData)
	{
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
		
		return "{\"ChargeID\":"+this.chargeId+",\"Date\":\""
			+dateFormatter.format(this.date)+"\","
			+"\"Current\":"+this.current+","
			+"\"MeasuredCap\":"+this.measuredCapacity+","
			+"\"nachgeladen\":"+ new Boolean(this.recharged).toString() + "}";
	}


	@Override
	public void parseFromJson(JSONObject json)
	{
		try
		{
			this.chargeId = json.getInt("ChargeID");
			DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
			this.date = dateFormatter.parse(json.getString("Date"));
			
			//TODO: float to double
			this.current = (float) json.getDouble("Current");
			this.measuredCapacity = (float) json.getDouble("MeasuredCap");
			this.recharged = json.getBoolean("nachgeladen");
		}
		catch(ParseException e)
		{
			e.printStackTrace();
		}
		catch(JSONException e)
		{
			e.printStackTrace();
		}
	}


	/**
	 * @return the chargeId
	 */
	public int getChargeId() {
		return chargeId;
	}


	/**
	 * @param chargeId the chargeId to set
	 */
	public void setChargeId(int chargeId) {
		this.chargeId = chargeId;
	}


	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}


	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}


	/**
	 * @return the current
	 */
	public float getCurrent() {
		return current;
	}


	/**
	 * @param current the current to set
	 */
	public void setCurrent(float current) {
		this.current = current;
	}


	/**
	 * @return the measuredCapacity
	 */
	public float getMeasuredCapacity() {
		return measuredCapacity;
	}


	/**
	 * @param measuredCapacity the measuredCapacity to set
	 */
	public void setMeasuredCapacity(float measuredCapacity) {
		this.measuredCapacity = measuredCapacity;
	}


	/**
	 * @return the recharged
	 */
	public boolean isRecharged() {
		return recharged;
	}


	/**
	 * @param recharged the recharged to set
	 */
	public void setRecharged(boolean recharged) {
		this.recharged = recharged;
	}
	
}
