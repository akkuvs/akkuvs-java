package mstrohm.akkuvs.dataclasses;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.json.JSONException;
import org.json.JSONObject;

public class Technology implements DataClass
{
	public int techId;
	public String name;
	
	
	/**
	 * @return the techId
	 */
	public int getTechId() {
		return techId;
	}
	/**
	 * @param techId the techId to set
	 */
	public void setTechId(int techId) {
		this.techId = techId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	

	public Technology()
	{
		this.techId = -1;
		this.name = "";
	}
	public Technology(int techId, String name)
	{
		this.techId = techId;
		this.name = name;
	}

	public Technology(String name)
	{
		this.techId = -1;
		this.name = name;
	}
	
	@Override
	public String toJSON(boolean allData)
	{
		return "{\"TechnologyID\":"+this.techId+",\"TechnologyName\":\""+this.name+"\"}";
	}
	@Override
	public void parseFromJson(JSONObject json)
	{
		try
		{
			this.techId = json.getInt("TechnologyID");
			this.name = json.getString("TechnologyName");
		}
		catch(JSONException e)
		{
			e.printStackTrace();
		}
		
	}
	
}
