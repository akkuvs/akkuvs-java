package mstrohm.akkuvs.dataclasses;

import org.json.JSONObject;

public class User implements DataClass {
	
	private int userId;
	private String userName;
	private String passwordHash;
	
	
	@Override
	public void parseFromJson(JSONObject json) {
		// TODO Auto-generated method stub

	}

	@Override
	public String toJSON(boolean allData) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the passwordHash
	 */
	public String getPasswordHash() {
		return passwordHash;
	}

	/**
	 * @param passwordHash the passwordHash to set
	 */
	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

}
