package mstrohm.akkuvs.dataclasses;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import mstrohm.akkuvs.dataclasses.*;

public class Battery implements DataClass
{
	public int id;
	public String regNr;
	public String name;
	public Technology technology;
	public boolean charged;
	public ArrayList<Charge> charges;
	public ArrayList<Discharge> discharges;
	public Size size;
	public float voltage;
	public int cells;
	public float capacity;
	public Date registrationDate;
	public ArrayList<Usage> usages;
	public ArrayList<State> states;
	public ArrayList<String> groups;
	
	public Battery()
	{
		this.id = 0;
		this.regNr = "";
		this.name = "";
		this.technology = new Technology();
		this.charged = false;
		this.charges = new ArrayList<Charge>();
		this.discharges = new ArrayList<Discharge>();
		this.size = new Size();
		this.voltage = 0;
		this.cells = 0;
		this.capacity = 0;
		this.registrationDate = new Date(0);
	}
	
	public Battery(int id, String label, String name, String technology)
	{
		this.id = id;
		this.regNr = label;
		this.name = name;
		this.technology = new Technology(technology);
		this.charges = new ArrayList<Charge>();
		this.discharges = new ArrayList<Discharge>();
		this.size = new Size();
		this.voltage = 0;
		this.cells = 0;
		this.capacity = 0;
		this.registrationDate = new Date(0);
	}

	public Battery(int id, String label, String name, Technology technology,
			Size size, float voltage, int cells, float capacity,
			Date registrationDate)
	{
		this.id = id;
		this.regNr = label;
		this.name = name;
		this.technology = technology;
		this.charges = new ArrayList<Charge>();
		this.discharges = new ArrayList<Discharge>();
		this.size = size;
		this.voltage = voltage;
		this.cells = cells;
		this.capacity = capacity;
		this.registrationDate = registrationDate;
	}
	
	
	@Override
	public void parseFromJson(JSONObject json)
	{
		try
		{
			DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
			this.id = json.getInt("ID");
			this.regNr = json.getString("RegNr");
			this.name = json.getString("Name");
			this.technology.parseFromJson(json.getJSONObject("Technology"));
			this.size.parseFromJson(json.getJSONObject("Size"));
			
			//TODO: change type from float to double
			this.voltage = (float) json.getDouble("Voltage");
			this.cells = json.getInt("Cells");
			this.charged = json.getBoolean("Charged");
			this.capacity = (float) json.getDouble("Capacity");
			
			this.registrationDate = dateFormatter.parse(json.getString("RegistrationDate"));
			
			this.charges = new ArrayList<Charge>();
			JSONArray jsonCharges = json.getJSONArray("Charges");
			
			for(int i = 0; i < jsonCharges.length(); i++)
			{
				Charge c = new Charge();
				c.parseFromJson(jsonCharges.getJSONObject(i));
				this.charges.add(c);
			}
			
			this.discharges = new ArrayList<Discharge>();
			JSONArray jsonDischarges = json.getJSONArray("Discharges");
			
			for(int i = 0; i < jsonDischarges.length(); i++)
			{
				Discharge d = new Discharge();
				d.parseFromJson(jsonDischarges.getJSONObject(i));
				this.discharges.add(d);
			}
			
			this.usages = new ArrayList<Usage>();
			JSONArray jsonUsages = json.getJSONArray("Usages");
			
			for(int i = 0; i < jsonUsages.length(); i++)
			{
				Usage u = new Usage();
				u.parseFromJson(jsonUsages.getJSONObject(i));
				this.usages.add(u);
			}
			
			this.states = new ArrayList<State>();
			JSONArray jsonStates = json.getJSONArray("States");
			
			for(int i = 0; i < jsonStates.length(); i++)
			{
				State s = new State();
				s.parseFromJson(jsonStates.getJSONObject(i));
				this.states.add(s);
			}
			
		}
		catch(ParseException e)
		{
			e.printStackTrace();
		}
		catch(JSONException e)
		{
			e.printStackTrace();
		}
	}
	
	
	@Override
	public String toJSON(boolean allData)
	{
		//String jsonString = "{";
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
		
		String jsonString = "{"
				+	"\"ID\":" + this.id
				+ ",\"RegNr\":\"" + this.regNr
				+ "\",\"Name\":\"" + this.name
				+ "\",\"Technology\":" + this.technology.toJSON(allData)
				+ ",\"Size\":" + this.size.toJSON(allData)
				+ ",\"Voltage\":" + this.voltage
				+ ",\"Cells\":" + this.cells
				+ ",\"Capacity\":" + this.capacity
				+ ",\"RegistrationDate\":\""
				+	dateFormatter.format(this.registrationDate)
				+ "\",";
		if(allData == true)
		{
			//charges and discharges will be sent too
			jsonString +=	"\"Charges\":[";
			for(int i = 0; i < this.charges.size(); i++)
			{
				if(i > 0)
				{
					jsonString += ",";
				}
				jsonString += this.charges.get(i).toJSON(allData);
			}
				
			jsonString +=	"],\"Discharges\":[";
			
			for(int i = 0; i < this.discharges.size(); i++)
			{
				if(i > 0)
				{
					jsonString += ",";
				}
				jsonString += this.discharges.get(i).toJSON(allData);
			}
			
			//TODO: Usages and States
			jsonString += "],\"Usages\":[";
			
			jsonString += "],\"States\":[";
			
			jsonString += "]";
		}
		else
		{
			jsonString += "\"Charges\":[],\"Discharges\":[],\"Usages\":[],\"States\":[]";
		}
		
		jsonString += "}";
		return jsonString;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the regNr
	 */
	public String getRegNr() {
		return regNr;
	}

	/**
	 * @param regNr the regNr to set
	 */
	public void setRegNr(String regNr) {
		this.regNr = regNr;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the technology
	 */
	public Technology getTechnology() {
		return technology;
	}

	/**
	 * @param technology the technology to set
	 */
	public void setTechnology(Technology technology) {
		this.technology = technology;
	}

	/**
	 * @return the charged
	 */
	public boolean isCharged() {
		return charged;
	}

	/**
	 * @param charged the charged to set
	 */
	public void setCharged(boolean charged) {
		this.charged = charged;
	}

	/**
	 * @return the charges
	 */
	public ArrayList<Charge> getCharges() {
		return charges;
	}

	/**
	 * @param charges the charges to set
	 */
	public void setCharges(ArrayList<Charge> charges) {
		this.charges = charges;
	}

	/**
	 * @return the discharges
	 */
	public ArrayList<Discharge> getDischarges() {
		return discharges;
	}

	/**
	 * @param discharges the discharges to set
	 */
	public void setDischarges(ArrayList<Discharge> discharges) {
		this.discharges = discharges;
	}

	/**
	 * @return the size
	 */
	public Size getSize() {
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(Size size) {
		this.size = size;
	}

	/**
	 * @return the voltage
	 */
	public float getVoltage() {
		return voltage;
	}

	/**
	 * @param voltage the voltage to set
	 */
	public void setVoltage(float voltage) {
		this.voltage = voltage;
	}

	/**
	 * @return the cells
	 */
	public int getCells() {
		return cells;
	}

	/**
	 * @param cells the cells to set
	 */
	public void setCells(int cells) {
		this.cells = cells;
	}

	/**
	 * @return the capacity
	 */
	public float getCapacity() {
		return capacity;
	}

	/**
	 * @param capacity the capacity to set
	 */
	public void setCapacity(float capacity) {
		this.capacity = capacity;
	}

	/**
	 * @return the registrationDate
	 */
	public Date getRegistrationDate() {
		return registrationDate;
	}

	/**
	 * @param registrationDate the registrationDate to set
	 */
	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	/**
	 * @return the usages
	 */
	public ArrayList<Usage> getUsages() {
		return usages;
	}

	/**
	 * @param usages the usages to set
	 */
	public void setUsages(ArrayList<Usage> usages) {
		this.usages = usages;
	}

	/**
	 * @return the states
	 */
	public ArrayList<State> getStates() {
		return states;
	}

	/**
	 * @param states the states to set
	 */
	public void setStates(ArrayList<State> states) {
		this.states = states;
	}

	/**
	 * @return the groups
	 */
	public ArrayList<String> getGroups() {
		return groups;
	}

	/**
	 * @param groups the groups to set
	 */
	public void setGroups(ArrayList<String> groups) {
		this.groups = groups;
	}

}
