package mstrohm.akkuvs.dataclasses;

import org.json.JSONObject;

public interface DataClass
{
	public void parseFromJson(JSONObject json);
	public String toJSON(boolean allData);
}
