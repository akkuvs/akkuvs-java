package mstrohm.akkuvs.dataclasses;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

public class Usage implements DataClass
{
	public int usageId;
	public String usageName;
	public int usageTypeId;
	public Date startDate;
	public String location;
	
	
	@Override
	public void parseFromJson(JSONObject json)
	{
		try
		{
			this.usageId = json.getInt("UsageID");
			this.usageName = json.getString("UsageName");
			DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
			this.startDate = dateFormatter.parse(json.getString("StartDate"));
			this.location = json.getString("Location");
		}
		catch(ParseException e)
		{
			e.printStackTrace();
		}
		catch(JSONException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public String toJSON(boolean allData)
	{
		return "{}";
	}

	/**
	 * @return the usageId
	 */
	public int getUsageId() {
		return usageId;
	}

	/**
	 * @param usageId the usageId to set
	 */
	public void setUsageId(int usageId) {
		this.usageId = usageId;
	}

	/**
	 * @return the usageName
	 */
	public String getUsageName() {
		return usageName;
	}

	/**
	 * @param usageName the usageName to set
	 */
	public void setUsageName(String usageName) {
		this.usageName = usageName;
	}

	/**
	 * @return the usageTypeId
	 */
	public int getUsageTypeId() {
		return usageTypeId;
	}

	/**
	 * @param usageTypeId the usageTypeId to set
	 */
	public void setUsageTypeId(int usageTypeId) {
		this.usageTypeId = usageTypeId;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}
	
}
