package mstrohm.akkuvs.dataclasses;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

public class Discharge implements DataClass
{
	public int dischargeId;
	public Date date;
	
	@Override
	public String toJSON(boolean allData)
	{
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
		
		return "{\"DischargeID\":"+this.dischargeId+",\"Date\":\""
			+dateFormatter.format(this.date)+"\"}";
	}

	@Override
	public void parseFromJson(JSONObject json)
	{
		try
		{
			this.dischargeId = json.getInt("DischargeID");
			DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
			this.date = dateFormatter.parse(json.getString("Date"));
		}
		catch (ParseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * @return the dischargeId
	 */
	public int getDischargeId() {
		return dischargeId;
	}

	/**
	 * @param dischargeId the dischargeId to set
	 */
	public void setDischargeId(int dischargeId) {
		this.dischargeId = dischargeId;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	
}
