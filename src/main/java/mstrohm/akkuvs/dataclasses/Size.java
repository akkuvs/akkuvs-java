package mstrohm.akkuvs.dataclasses;

import org.json.JSONException;
import org.json.JSONObject;

public class Size implements DataClass
{
	public int sizeId;
	public String name;
	
	public Size()
	{
		this.sizeId = -1;
		this.name = "";
	}

	public Size(String name)
	{
		this.name = name;
	}
	
	public Size(int sizeId, String name)
	{
		this.sizeId = sizeId;
		this.name = name;
	}

	/**
	 * @return the sizeId
	 */
	public int getSizeId() {
		return sizeId;
	}

	/**
	 * @param sizeId the sizeId to set
	 */
	public void setSizeId(int sizeId) {
		this.sizeId = sizeId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	
	
	@Override
	public String toJSON(boolean allData)
	{
		
		return "{\"SizeID\":"+this.sizeId+",\"SizeName\":\""+this.name+"\"}";
	}

	@Override
	public void parseFromJson(JSONObject json)
	{
		try
		{
			this.sizeId = json.getInt("SizeID");
			this.name = json.getString("SizeName");
		}
		catch(JSONException e)
		{
			e.printStackTrace();
		}
	}
	
}
