package mstrohm.akkuvs;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateHelper {
	private static final SessionFactory sessionFactory = HibernateHelper.buildSessionFactory();

	private static SessionFactory buildSessionFactory() {
		try {
			return new Configuration().configure().buildSessionFactory();
		}
		catch (Throwable e) {
			System.err.println("ERROR: Hibernate SessionFactory couldn't be created! Reason: " + e.toString());
			throw new ExceptionInInitializerError(e);
		}
		
	}
	
	public static SessionFactory getSessionFactory() {
		return HibernateHelper.sessionFactory;
	}
	
	
}
