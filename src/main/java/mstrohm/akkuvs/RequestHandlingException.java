package mstrohm.akkuvs;

public class RequestHandlingException extends Exception
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String reason;
	public RequestHandlingException(String reason)
	{
		this.reason = reason;
	}
	public RequestHandlingException()
	{
		this.reason = "";
	}
}
