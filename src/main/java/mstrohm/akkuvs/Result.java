package mstrohm.akkuvs;


import java.util.ArrayList;

import mstrohm.akkuvs.dataclasses.DataClass;

public class Result
{
	public short ErrorCode;
	public String ErrorValue;
	
	public ArrayList<DataClass> Data;
	
	private boolean allData;

	public Result()
	{
		this.ErrorCode = 0;
		this.ErrorValue = "";
		this.Data = new ArrayList<DataClass>();
		this.allData = true;
	}
	
	public void printAllData(boolean allData)
	{
		this.allData = allData;
	}
	
	public String toJSON()
	{
		String jsonString = "{\"ErrorCode\":"+this.ErrorCode
				+ ",\"ErrorValue\":\"" + this.ErrorValue
				+ "\",\"Data\":[";
		for(int i = 0; i < this.Data.size(); i++)
		{
			if(i>0)
			{
				jsonString += ",";
			}
			jsonString += this.Data.get(i).toJSON(this.allData);
		}
		
		jsonString += "]}";
		
		return jsonString;
	}
}
