
if [ $# -le 0 ]
then
  echo "No files/directory specified to be added!"
else
  echo "BZR:"
  bzr add $@
  echo "GIT:"
  git add $@
  echo "DONE!"
fi
