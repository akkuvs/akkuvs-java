SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `AkkuDB_new` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `AkkuDB_new` ;

-- -----------------------------------------------------
-- Table `AkkuDB_new`.`Sizes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AkkuDB_new`.`Sizes` ;

CREATE  TABLE IF NOT EXISTS `AkkuDB_new`.`Sizes` (
  `SizeID` SMALLINT NOT NULL ,
  `Name` VARCHAR(40) NOT NULL ,
  PRIMARY KEY (`SizeID`) )
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `AkkuDB_new`.`Technology`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AkkuDB_new`.`Technology` ;

CREATE  TABLE IF NOT EXISTS `AkkuDB_new`.`Technology` (
  `TechnologyID` SMALLINT NOT NULL ,
  `Name` VARCHAR(32) NOT NULL ,
  PRIMARY KEY (`TechnologyID`) )
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `AkkuDB_new`.`UsageNames`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AkkuDB_new`.`UsageNames` ;

CREATE  TABLE IF NOT EXISTS `AkkuDB_new`.`UsageNames` (
  `UsageID` SMALLINT NOT NULL ,
  `Name` VARCHAR(20) NOT NULL ,
  PRIMARY KEY (`UsageID`) )
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `AkkuDB_new`.`StateNames`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AkkuDB_new`.`StateNames` ;

CREATE  TABLE IF NOT EXISTS `AkkuDB_new`.`StateNames` (
  `StateID` SMALLINT NOT NULL ,
  `Name` VARCHAR(20) NOT NULL ,
  PRIMARY KEY (`StateID`) )
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `AkkuDB_new`.`Battery`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AkkuDB_new`.`Battery` ;

CREATE  TABLE IF NOT EXISTS `AkkuDB_new`.`Battery` (
  `BatteryID` INT NOT NULL AUTO_INCREMENT ,
  `RegistrationNumber` VARCHAR(40) NOT NULL ,
  `Name` VARCHAR(64) NOT NULL ,
  `Technology` SMALLINT NOT NULL ,
  `Size` SMALLINT NOT NULL ,
  `Voltage` FLOAT NOT NULL ,
  `Cells` SMALLINT NOT NULL ,
  `Capacity` FLOAT NOT NULL ,
  `RegDate` TIMESTAMP NOT NULL ,
  PRIMARY KEY (`BatteryID`) ,
  INDEX `FK_Technology` (`Technology` ASC) ,
  INDEX `FK_Size` (`Size` ASC) ,
  CONSTRAINT `FK_Technology`
    FOREIGN KEY (`Technology` )
    REFERENCES `AkkuDB_new`.`Technology` (`TechnologyID` )
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `FK_Size`
    FOREIGN KEY (`Size` )
    REFERENCES `AkkuDB_new`.`Sizes` (`SizeID` )
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `AkkuDB_new`.`Discharges`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AkkuDB_new`.`Discharges` ;

CREATE  TABLE IF NOT EXISTS `AkkuDB_new`.`Discharges` (
  `BatteryID` INT NOT NULL ,
  `DischargeDate` TIMESTAMP NOT NULL ,
  INDEX `FK_Discharges_BatteryID` (`BatteryID` ASC) ,
  PRIMARY KEY (`BatteryID`, `DischargeDate`) ,
  CONSTRAINT `FK_Discharges_BatteryID`
    FOREIGN KEY (`BatteryID` )
    REFERENCES `AkkuDB_new`.`Battery` (`BatteryID` ))
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `AkkuDB_new`.`Charges`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AkkuDB_new`.`Charges` ;

CREATE  TABLE IF NOT EXISTS `AkkuDB_new`.`Charges` (
  `BatteryID` INT NOT NULL ,
  `ChargeDate` TIMESTAMP NOT NULL ,
  `Current` FLOAT NOT NULL DEFAULT 0 ,
  `MeasuredCap` FLOAT NOT NULL DEFAULT 0 ,
  `nachgeladen` TINYINT(1) NOT NULL DEFAULT false ,
  PRIMARY KEY (`BatteryID`, `ChargeDate`) ,
  INDEX `FK_Charges_BatteryID` (`BatteryID` ASC) ,
  CONSTRAINT `FK_Charges_BatteryID`
    FOREIGN KEY (`BatteryID` )
    REFERENCES `AkkuDB_new`.`Battery` (`BatteryID` )
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `AkkuDB_new`.`States`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AkkuDB_new`.`States` ;

CREATE  TABLE IF NOT EXISTS `AkkuDB_new`.`States` (
  `BatteryID` INT NOT NULL ,
  `StateStartDate` TIMESTAMP NOT NULL ,
  `State` SMALLINT NOT NULL ,
  PRIMARY KEY (`BatteryID`, `StateStartDate`) ,
  INDEX `FK_States_BatteryID` (`BatteryID` ASC) ,
  INDEX `FK_States_StateNames` (`State` ASC) ,
  CONSTRAINT `FK_States_BatteryID`
    FOREIGN KEY (`BatteryID` )
    REFERENCES `AkkuDB_new`.`Battery` (`BatteryID` )
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `FK_States_StateNames`
    FOREIGN KEY (`State` )
    REFERENCES `AkkuDB_new`.`StateNames` (`StateID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `AkkuDB_new`.`Usages`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AkkuDB_new`.`Usages` ;

CREATE  TABLE IF NOT EXISTS `AkkuDB_new`.`Usages` (
  `BatteryID` INT NOT NULL ,
  `StartDate` TIMESTAMP NOT NULL ,
  `Usage` SMALLINT NOT NULL ,
  `Place` VARCHAR(64) NOT NULL ,
  PRIMARY KEY (`BatteryID`, `StartDate`) ,
  INDEX `FK_Usages_BatteryID` (`BatteryID` ASC) ,
  INDEX `FK_Usages_UsageNames` (`Usage` ASC) ,
  CONSTRAINT `FK_Usages_BatteryID`
    FOREIGN KEY (`BatteryID` )
    REFERENCES `AkkuDB_new`.`Battery` (`BatteryID` )
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `FK_Usages_UsageNames`
    FOREIGN KEY (`Usage` )
    REFERENCES `AkkuDB_new`.`UsageNames` (`UsageID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `AkkuDB_new`.`FormerBatteries`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AkkuDB_new`.`FormerBatteries` ;

CREATE  TABLE IF NOT EXISTS `AkkuDB_new`.`FormerBatteries` (
  `FormerBatteryID` INT NOT NULL AUTO_INCREMENT ,
  `RegistrationNumber` VARCHAR(40) NOT NULL ,
  `Name` VARCHAR(64) NOT NULL ,
  `TechnologyName` VARCHAR(32) NOT NULL ,
  `SizeName` VARCHAR(40) NOT NULL ,
  `Voltage` FLOAT NOT NULL ,
  `Cells` SMALLINT NOT NULL ,
  `Capacity` FLOAT NOT NULL ,
  `RegDate` TIMESTAMP NOT NULL ,
  `UnregDate` TIMESTAMP NOT NULL ,
  `UnregReason` TEXT NULL ,
  PRIMARY KEY (`FormerBatteryID`) )
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `AkkuDB_new`.`BatteryGroup`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AkkuDB_new`.`BatteryGroup` ;

CREATE  TABLE IF NOT EXISTS `AkkuDB_new`.`BatteryGroup` (
  `GroupID` INT NOT NULL AUTO_INCREMENT ,
  `Name` VARCHAR(40) NOT NULL ,
  PRIMARY KEY (`GroupID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AkkuDB_new`.`GroupBatteries`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AkkuDB_new`.`GroupBatteries` ;

CREATE  TABLE IF NOT EXISTS `AkkuDB_new`.`GroupBatteries` (
  `GroupID` INT NOT NULL ,
  `BatteryID` INT NOT NULL ,
  PRIMARY KEY (`GroupID`, `BatteryID`) ,
  INDEX `FK_GroupBatteries_BatteryID` (`BatteryID` ASC) ,
  INDEX `FK_GroupBatteries_GroupID` (`GroupID` ASC) ,
  CONSTRAINT `FK_GroupBatteries_GroupID`
    FOREIGN KEY (`GroupID` )
    REFERENCES `AkkuDB_new`.`BatteryGroup` (`GroupID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_GroupBatteries_BatteryID`
    FOREIGN KEY (`BatteryID` )
    REFERENCES `AkkuDB_new`.`Battery` (`BatteryID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
