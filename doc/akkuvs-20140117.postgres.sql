
-- Dumped schema, created with PgAdmin3
-- Started on 2014-01-17 10:15:02 CET


SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;




CREATE TABLE "Size" (
    "SizeID" integer NOT NULL,
    "Name" character varying(255)
);
ALTER TABLE ONLY "Size"
    ADD CONSTRAINT "SizeID" PRIMARY KEY ("SizeID");
CREATE SEQUENCE "Size_SizeID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE "Size_SizeID_seq" OWNED BY "Size"."SizeID";
ALTER TABLE ONLY "Size" ALTER COLUMN "SizeID" SET DEFAULT nextval('"Size_SizeID_seq"'::regclass);




CREATE TABLE "Technology" (
    "TechnologyID" integer NOT NULL,
    "Name" character varying(64) NOT NULL
    
);
ALTER TABLE ONLY "Technology"
    ADD CONSTRAINT "TechnologyID" PRIMARY KEY ("TechnologyID");
CREATE SEQUENCE "Technology_TechnologyID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE "Technology_TechnologyID_seq" OWNED BY "Technology"."TechnologyID";
ALTER TABLE ONLY "Technology" ALTER COLUMN "TechnologyID" SET DEFAULT nextval('"Technology_TechnologyID_seq"'::regclass);




CREATE TABLE "Battery" (
    "BatteryID" integer NOT NULL,
    "RegistrationNumber" character varying(16) NOT NULL,
    "Name" character varying(64),
    "Voltage" double precision NOT NULL,
    "Technology" integer NOT NULL,
    "Size" integer NOT NULL,
    "Cells" integer NOT NULL,
    "Capacity" double precision NOT NULL,
    "RegistrationDate" date NOT NULL
);
ALTER TABLE ONLY "Battery"
    ADD CONSTRAINT "BatteryID" PRIMARY KEY ("BatteryID");
ALTER TABLE ONLY "Battery"
    ADD CONSTRAINT "Size" FOREIGN KEY ("Size") REFERENCES "Size"("SizeID");
ALTER TABLE ONLY "Battery"
    ADD CONSTRAINT "Technology" FOREIGN KEY ("Technology") REFERENCES "Technology"("TechnologyID");

CREATE SEQUENCE "Battery_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE "Battery_ID_seq" OWNED BY "Battery"."BatteryID";
ALTER TABLE ONLY "Battery" ALTER COLUMN "BatteryID" SET DEFAULT nextval('"Battery_ID_seq"'::regclass);

COMMENT ON COLUMN "Battery"."Capacity" IS 'Capacity of the battery in mAh.';




CREATE TABLE "Charge" (
    "ChargeID" integer NOT NULL,
    "BatteryID" integer NOT NULL,
    "ChargeDate" date NOT NULL,
    "Current" double precision,
    "MeasuredCapacity" double precision,
    "Recharged" boolean NOT NULL
);
ALTER TABLE ONLY "Charge"
    ADD CONSTRAINT "ChargeID" PRIMARY KEY ("ChargeID");
ALTER TABLE ONLY "Charge"
    ADD CONSTRAINT "BatteryID" FOREIGN KEY ("BatteryID") REFERENCES "Battery"("BatteryID");

CREATE SEQUENCE "Charge_ChargeID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE "Charge_ChargeID_seq" OWNED BY "Charge"."ChargeID";
ALTER TABLE ONLY "Charge" ALTER COLUMN "ChargeID" SET DEFAULT nextval('"Charge_ChargeID_seq"'::regclass);




CREATE TABLE "Discharge" (
    "DischargeID" integer NOT NULL,
    "BatteryID" integer NOT NULL,
    "DischargeDate" date NOT NULL
);
ALTER TABLE ONLY "Discharge"
    ADD CONSTRAINT "DischargeID" PRIMARY KEY ("DischargeID");
ALTER TABLE ONLY "Discharge"
    ADD CONSTRAINT "BatteryID" FOREIGN KEY ("BatteryID") REFERENCES "Battery"("BatteryID");

CREATE SEQUENCE "Discharge_DischargeID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE "Discharge_DischargeID_seq" OWNED BY "Discharge"."DischargeID";
ALTER TABLE ONLY "Discharge" ALTER COLUMN "DischargeID" SET DEFAULT nextval('"Discharge_DischargeID_seq"'::regclass);




CREATE TABLE "StateTypes" (
    "StateTypeID" integer NOT NULL,
    "StateTypeName" character varying(64) NOT NULL
);
ALTER TABLE ONLY "StateTypes"
    ADD CONSTRAINT "StateTypeID" PRIMARY KEY ("StateTypeID");
CREATE SEQUENCE "StateTypes_StateTypeID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE "StateTypes_StateTypeID_seq" OWNED BY "StateTypes"."StateTypeID";
ALTER TABLE ONLY "StateTypes" ALTER COLUMN "StateTypeID" SET DEFAULT nextval('"StateTypes_StateTypeID_seq"'::regclass);

COMMENT ON TABLE "StateTypes" IS 'StateTypes holds the state names to have unique state names across all batteries.';




CREATE TABLE "State" (
    "StateID" integer NOT NULL,
    "BatteryID" integer NOT NULL,
    "StateStartDate" date NOT NULL,
    "StateType" integer NOT NULL
);
ALTER TABLE ONLY "State"
    ADD CONSTRAINT "StateID" PRIMARY KEY ("StateID");
ALTER TABLE ONLY "State"
    ADD CONSTRAINT "BatteryID" FOREIGN KEY ("BatteryID") REFERENCES "Battery"("BatteryID");

CREATE SEQUENCE "State_StateID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE "State_StateID_seq" OWNED BY "State"."StateID";
ALTER TABLE ONLY "State" ALTER COLUMN "StateID" SET DEFAULT nextval('"State_StateID_seq"'::regclass);

COMMENT ON TABLE "State" IS 'States hold the states each battery is in.';




CREATE TABLE "UsageTypes" (
    "UsageTypeID" integer NOT NULL,
    "UsageTypeName" character varying(64) NOT NULL
);
ALTER TABLE ONLY "UsageTypes"
    ADD CONSTRAINT "UsageTypeID" PRIMARY KEY ("UsageTypeID");
CREATE SEQUENCE "UsageTypes_UsageTypeID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE "UsageTypes_UsageTypeID_seq" OWNED BY "UsageTypes"."UsageTypeID";
ALTER TABLE ONLY "UsageTypes" ALTER COLUMN "UsageTypeID" SET DEFAULT nextval('"UsageTypes_UsageTypeID_seq"'::regclass);

COMMENT ON TABLE "UsageTypes" IS 'UsageTypes holds the usage type names to have the same usage type names across all batteries.';




CREATE TABLE "Usage" (
    "UsageID" integer NOT NULL,
    "BatteryID" integer NOT NULL,
    "UsageType" integer NOT NULL,
    "UsagePlace" character varying(64) NOT NULL,
    "UsageStartDate" date NOT NULL
);
ALTER TABLE ONLY "Usage"
    ADD CONSTRAINT "UsageID" PRIMARY KEY ("UsageID");
ALTER TABLE ONLY "Usage"
    ADD CONSTRAINT "BatteryID" FOREIGN KEY ("BatteryID") REFERENCES "Battery"("BatteryID");
ALTER TABLE ONLY "Usage"
    ADD CONSTRAINT "UsageType" FOREIGN KEY ("UsageType") REFERENCES "UsageTypes"("UsageTypeID");

CREATE SEQUENCE "Usage_UsageID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE "Usage_UsageID_seq" OWNED BY "Usage"."UsageID";
ALTER TABLE ONLY "Usage" ALTER COLUMN "UsageID" SET DEFAULT nextval('"Usage_UsageID_seq"'::regclass);

COMMENT ON TABLE "Usage" IS 'Holds informations of the usage for each battery.';




CREATE TABLE "Users" (
    "UserID" integer NOT NULL,
    "UserName" character varying(64) NOT NULL,
    "PasswordHash" character(256) NOT NULL
);
ALTER TABLE ONLY "Users"
    ADD CONSTRAINT "UserID" PRIMARY KEY ("UserID");
CREATE SEQUENCE "Users_UserID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE "Users_UserID_seq" OWNED BY "Users"."UserID";
ALTER TABLE ONLY "Users" ALTER COLUMN "UserID" SET DEFAULT nextval('"Users_UserID_seq"'::regclass);

--
-- PostgreSQL database dump complete
--

