-- Database: "AkkuVS"

DROP DATABASE IF EXISTS "AkkuVS";

CREATE DATABASE "AkkuVS"
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       CONNECTION LIMIT = -1;

-- -----------------------------------------------------
-- Table AkkuVS.Sizes
-- -----------------------------------------------------
DROP TABLE IF EXISTS AkkuVS.Sizes ;

CREATE  TABLE IF NOT EXISTS AkkuVS.Sizes (
  SizeID SMALLINT NOT NULL ,
  Name VARCHAR(40) NOT NULL ,
  PRIMARY KEY (SizeID) );


-- -----------------------------------------------------
-- Table AkkuVS.Technology
-- -----------------------------------------------------
DROP TABLE IF EXISTS AkkuVS.Technology ;

CREATE  TABLE IF NOT EXISTS AkkuVS.Technology (
  TechnologyID SMALLINT NOT NULL ,
  Name VARCHAR(32) NOT NULL ,
  PRIMARY KEY (TechnologyID) );


-- -----------------------------------------------------
-- Table AkkuVS.UsageNames
-- -----------------------------------------------------
DROP TABLE IF EXISTS AkkuVS.UsageNames ;

CREATE  TABLE IF NOT EXISTS AkkuVS.UsageNames (
  UsageID SMALLINT NOT NULL ,
  Name VARCHAR(20) NOT NULL ,
  PRIMARY KEY (UsageID) );


-- -----------------------------------------------------
-- Table AkkuVS.StateNames
-- -----------------------------------------------------
DROP TABLE IF EXISTS AkkuVS.StateNames ;

CREATE  TABLE IF NOT EXISTS AkkuVS.StateNames (
  StateID SMALLINT NOT NULL ,
  Name VARCHAR(20) NOT NULL ,
  PRIMARY KEY (StateID) );


-- -----------------------------------------------------
-- Table AkkuVS.Battery
-- -----------------------------------------------------
DROP TABLE IF EXISTS AkkuVS.Battery ;

CREATE  TABLE IF NOT EXISTS AkkuVS.Battery (
  BatteryID INT NOT NULL UNIQUE,
  RegistrationNumber VARCHAR(40) NOT NULL ,
  Name VARCHAR(64) NOT NULL ,
  Technology SMALLINT NOT NULL ,
  Size SMALLINT NOT NULL ,
  Voltage FLOAT NOT NULL ,
  Cells SMALLINT NOT NULL ,
  Capacity FLOAT NOT NULL ,
  RegDate TIMESTAMP NOT NULL ,
  PRIMARY KEY (BatteryID) ,
  INDEX FK_Technology (Technology) ,
  INDEX FK_Size (Size) ,
  CONSTRAINT FK_Technology
    FOREIGN KEY (Technology )
    REFERENCES AkkuVS.Technology (TechnologyID )
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT FK_Size
    FOREIGN KEY (Size )
    REFERENCES AkkuVS.Sizes (SizeID )
    ON DELETE RESTRICT
    ON UPDATE RESTRICT);

-- -----------------------------------------------------
-- Table AkkuVS.Discharges
-- -----------------------------------------------------
DROP TABLE IF EXISTS AkkuVS.Discharges ;

CREATE  TABLE IF NOT EXISTS AkkuVS.Discharges (
  BatteryID INT NOT NULL ,
  DischargeDate TIMESTAMP NOT NULL ,
  INDEX FK_Discharges_BatteryID (BatteryID) ,
  PRIMARY KEY (BatteryID, DischargeDate) ,
  CONSTRAINT FK_Discharges_BatteryID
    FOREIGN KEY (BatteryID )
    REFERENCES AkkuVS.Battery (BatteryID ));


-- -----------------------------------------------------
-- Table AkkuVS.Charges
-- -----------------------------------------------------
DROP TABLE IF EXISTS AkkuVS.Charges ;

CREATE  TABLE IF NOT EXISTS AkkuVS.Charges (
  BatteryID INT NOT NULL ,
  ChargeDate TIMESTAMP NOT NULL ,
  Current FLOAT NOT NULL DEFAULT 0 ,
  MeasuredCap FLOAT NOT NULL DEFAULT 0 ,
  nachgeladen TINYINT(1) NOT NULL DEFAULT false ,
  PRIMARY KEY (BatteryID, ChargeDate) ,
  INDEX FK_Charges_BatteryID (BatteryID) ,
  CONSTRAINT FK_Charges_BatteryID
    FOREIGN KEY (BatteryID )
    REFERENCES AkkuVS.Battery (BatteryID )
    ON DELETE RESTRICT
    ON UPDATE RESTRICT);


-- -----------------------------------------------------
-- Table AkkuVS.States
-- -----------------------------------------------------
DROP TABLE IF EXISTS AkkuVS.States ;

CREATE  TABLE IF NOT EXISTS AkkuVS.States (
  BatteryID INT NOT NULL ,
  StateStartDate TIMESTAMP NOT NULL ,
  State SMALLINT NOT NULL ,
  PRIMARY KEY (BatteryID, StateStartDate) ,
  INDEX FK_States_BatteryID (BatteryID) ,
  INDEX FK_States_StateNames (State) ,
  CONSTRAINT FK_States_BatteryID
    FOREIGN KEY (BatteryID )
    REFERENCES AkkuVS.Battery (BatteryID )
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT FK_States_StateNames
    FOREIGN KEY (State )
    REFERENCES AkkuVS.StateNames (StateID )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table AkkuVS.Usages
-- -----------------------------------------------------
DROP TABLE IF EXISTS AkkuVS.Usages ;

CREATE  TABLE IF NOT EXISTS AkkuVS.Usages (
  BatteryID INT NOT NULL ,
  StartDate TIMESTAMP NOT NULL ,
  Usage SMALLINT NOT NULL ,
  Place VARCHAR(64) NOT NULL ,
  PRIMARY KEY (BatteryID, StartDate) ,
  INDEX FK_Usages_BatteryID (BatteryID) ,
  INDEX FK_Usages_UsageNames (Usage) ,
  CONSTRAINT FK_Usages_BatteryID
    FOREIGN KEY (BatteryID )
    REFERENCES AkkuVS.Battery (BatteryID )
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT FK_Usages_UsageNames
    FOREIGN KEY (Usage )
    REFERENCES AkkuVS.UsageNames (UsageID )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table AkkuVS.FormerBatteries
-- -----------------------------------------------------
DROP TABLE IF EXISTS AkkuVS.FormerBatteries ;

CREATE  TABLE IF NOT EXISTS AkkuVS.FormerBatteries (
  FormerBatteryID INT NOT NULL,
  RegistrationNumber VARCHAR(40) NOT NULL ,
  Name VARCHAR(64) NOT NULL ,
  TechnologyName VARCHAR(32) NOT NULL ,
  SizeName VARCHAR(40) NOT NULL ,
  Voltage FLOAT NOT NULL ,
  Cells SMALLINT NOT NULL ,
  Capacity FLOAT NOT NULL ,
  RegDate TIMESTAMP NOT NULL ,
  UnregDate TIMESTAMP NOT NULL ,
  UnregReason TEXT NULL ,
  PRIMARY KEY (FormerBatteryID) );


-- -----------------------------------------------------
-- Table AkkuVS.BatteryGroup
-- -----------------------------------------------------
DROP TABLE IF EXISTS AkkuVS.BatteryGroup ;

CREATE  TABLE IF NOT EXISTS AkkuVS.BatteryGroup (
  GroupID INT NOT NULL,
  Name VARCHAR(40) NOT NULL ,
  PRIMARY KEY (GroupID) );


-- -----------------------------------------------------
-- Table AkkuVS.GroupBatteries
-- -----------------------------------------------------
DROP TABLE IF EXISTS AkkuVS.GroupBatteries ;

CREATE  TABLE IF NOT EXISTS AkkuVS.GroupBatteries (
  GroupID INT NOT NULL ,
  BatteryID INT NOT NULL ,
  PRIMARY KEY (GroupID, BatteryID) ,
  INDEX FK_GroupBatteries_BatteryID (BatteryID) ,
  INDEX FK_GroupBatteries_GroupID (GroupID) ,
  CONSTRAINT FK_GroupBatteries_GroupID
    FOREIGN KEY (GroupID )
    REFERENCES AkkuVS.BatteryGroup (GroupID )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT FK_GroupBatteries_BatteryID
    FOREIGN KEY (BatteryID )
    REFERENCES AkkuVS.Battery (BatteryID )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


